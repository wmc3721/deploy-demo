[![pipeline status](http://47.104.146.120/wmc3721/deploy-demo/badges/master/pipeline.svg)](http://47.104.146.120/wmc3721/deploy-demo/commits/master)

### 1 What is CI/CD
[持续集成是什么? ——阮一峰](http://www.ruanyifeng.com/blog/2015/09/continuous-integration.html)

### 2 Configure Gitlab CI
+ [添加```.gitlab-ci.yml```到项目根目录](https://gitlab.com/help/ci/quick_start/README#creating-a-gitlab-ciyml-file)
+ [配置一个```Runner```](https://gitlab.com/help/ci/quick_start/README#configuring-a-runner)
  + [Install it](https://docs.gitlab.com/runner/install/)
  + [Configure it](https://gitlab.com/help/ci/runners/README.md#registering-a-specific-runner)


For more detail: [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)

### 3 Example
申请2台阿里云机
| 系统 | 内存 | CPU |
|------|------|-----|
|Ubuntu18.04|4GB|2核|

#### Install the Runner
1.Add GitLab’s official repository:
```
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
```
2.Install the latest version of GitLab Runner, or skip to the next step to install a specific version:
```
sudo apt-get install gitlab-runner
```

For more detail: [使用官方的GitLab库安装GitLab Runner](https://docs.gitlab.com/runner/install/linux-repository.html)

#### Register the Runner
##### Requirements
+ You have installed the runner
+ Obtain a token for a shared or specific Runner via GitLab’s interface
![Image text](doc/img/runner.png)

##### Register
![Image text](doc/img/register.png)

For more detail: [Registering Runners](https://docs.gitlab.com/runner/register/index.html)

##### Wait runners active
![Image text](doc/img/active.png)

##### Add .gitlab-ci.yml
```
# 定义build和deploy stage
stages:
    - build
    - deploy

# job名
build-job:
    # build-job归属于build stage,同一个stage下的不同job并行执行
    stage: build
    # 此任务会跑在tag为build-server的runner上
    tags: 
        - build-server
    script:
        - ROOT_PATH=`pwd`
        # use nodejs of green version
        - source CI/use_nodejs.sh
        - bash code/web/ci/generate_dist.sh # compile ui component
        # use tmp folder to zip artifact
        - rm -rf tmp
        - mkdir -p tmp/web
        - cd tmp
        - cp -r ../code/web/build/* ./web
        - cp ${NODE_BIN_PATH}/node .
        - tar -czf demo.tar.gz *
        - mv demo.tar.gz $ROOT_PATH/.
    artifacts:
        # build出来的包1天后删除
        expire_in: 1 day
        paths:
            - demo.tar.gz

deploy-job:
    stage: deploy
    tags: 
        - deploy-server
    script:
        - sudo pkill -9 node
        - tar -zxvf demo.tar.gz
        - bash CI/start_up.sh
    # 只有在master分支变更时triggle
    only:
        - master
```

把yml文件push上去
![Image text](doc/img/home-run.png)

pipeline的详细页面，可以看到job的图形化界面
![Image text](doc/img/pipeline-detail.png)

构建完成后网站自动部署到阿里云机上
![Image text](doc/img/deploy-page.png)

### 4 搭建自己的Gitlab
[Download a GitLab Omnibus package(recommended installation)](https://about.gitlab.com/install/#ubuntu)

然后搭建完成
![Image text](doc/img/gitlab-install.png)

注册登录
![Image text](doc/img/gitlab-login.png)

进入首页，接下来可以创建项目
![Image text](doc/img/gitlab-home.png)