#!/bin/bash
export CUR_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export ROOT_PATH=${CUR_PATH}/..

export SOFTWARE_FOLDER=${ROOT_PATH}/software
export NODE_BIN_PATH=${SOFTWARE_FOLDER}/node-v10.15.3-linux-x64/bin
tar -xvf ${SOFTWARE_FOLDER}/node-*.tar.xz -C ${SOFTWARE_FOLDER}/ >/dev/null 2>&1
export PATH=$PATH:${NODE_BIN_PATH}
npm config set registry https://registry.npm.taobao.org