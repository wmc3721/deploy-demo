#!/bin/bash

CUR_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
ROOT_PATH=${CUR_PATH}/..

cd ${ROOT_PATH}
chmod +x node
nohup ./node web/server.js >/dev/null 2>&1 &