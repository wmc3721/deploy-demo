#!/bin/bash

export WEB_CI_PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export WEB_ROOT_PATH=${WEB_CI_PATH}/../
export WEB_BUILD_PATH=${WEB_ROOT_PATH}/build
export WEB_CLIENT_PATH=${WEB_ROOT_PATH}/client
export WEB_SERVER_PATH=${WEB_ROOT_PATH}/server
export WEB_SERVER_DIST=${WEB_SERVER_PATH}/dist

rm -rf ${WEB_BUILD_PATH}
mkdir -p ${WEB_BUILD_PATH}/public

cd ${WEB_CLIENT_PATH}

echo "============== start install client dependencies =============="

npm install --unsafe-perm=true --allow-root
wait

echo "============== end install client dependencies =============="

echo "============== start build client =============="

npm run build
wait

echo "============== end build client =============="

cp -rf ./dist/client/* ${WEB_BUILD_PATH}/public

cd ${WEB_SERVER_PATH}/

echo "============== start install server dependencies =============="

npm install
wait

echo "============== end install server dependencies =============="

echo "============== start build ui server =============="

npm run build
wait

echo "============== end build ui server =============="

cp -rf ${WEB_SERVER_DIST}/* ${WEB_BUILD_PATH}/
