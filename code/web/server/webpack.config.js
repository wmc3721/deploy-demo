const path = require('path');

module.exports = {
    mode: 'production',
    entry: './bin/www',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'server.js',
        chunkFilename: '[name].chunk.js',
        libraryTarget: 'commonjs',
    },
    node: {
        fs: 'empty',
        child_process: 'empty',
        tls: 'empty',
        net: 'empty',
        cluster: 'empty',
        __filename: false,
        __dirname: false,

    },
    target: 'node',
};
