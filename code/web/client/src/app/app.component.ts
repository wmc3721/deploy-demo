import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: any;
  constructor(private http: HttpClient) { }
  async ngOnInit() {
    this.title = (await this.http.get('/hello').toPromise() as any).str;
    console.log(this.title);
  }
}
